# Fancy Gallery
## A simple Addon for SP Page Builder

### To install just:
- Download the zip file and uncompress
- Create a folder in components/com_sppagebuilder/addons with the name fancy_gallery
- Copy the files in the folder fancy_gallery
